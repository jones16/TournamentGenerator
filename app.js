//Imports

var express = require('express');
var layout = require('express-layout');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var helmet = require ('helmet');
var hbs = require ('express-handlebars');
const authRoutes = require('./routes/oauth');
const passportSetup = require('./config/passport-setup');

//DB
var mongoose = require('mongoose');
mongoose.connect('mongodb://84.183.237.237:27017/data/db', {useMongoClient: true});
mongoose.Promise = global.Promise;


//declare routes
var index = require('./routes/index');
var users = require('./routes/users');
//var test = require('./routes/test');
var request = require('./routes/request');
var login = require('./routes/users');
var tournament = require('./routes/tournament');

//Initialisierung --> erstellt req und res
var app = express();

// view engine setup
app.engine('handlebars', hbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(helmet());
app.use('/auth', authRoutes);

//mapping von index und user
app.use('/', index);
app.use('/tournament', tournament);
//app.use('/test', test);
app.use('/request', request);
app.use('/login', users);
app.use(layout());

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
