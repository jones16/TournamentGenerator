//oauth functionality

const router = require('express').Router();
const passport = require('passport');

//auth login
router.get('/login', (req,res) => {
    console.log('login oauth')
    res.render('login');
});

// auth logout
router.get('/logout', (req, res) =>{
    // handle with passport
    console.log('logging out oauth');
    res.send('logging out');
})

// auth with google
router.get('/google', passport.authenticate('google', {
    scope: ['profile']
}));

// callback route fpr google to redirect to
router.get('/google/redirect', (req, res) => {
    res.send('you reached the callback URI')
    console.log('google redirect');
});

module.exports = router;
