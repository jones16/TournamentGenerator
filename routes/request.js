//unused
//example request route for demo purposes

var express = require('express');
var router = express.Router();
var request = require('request-promise');
var colors = require('colors');

var options = {
    uri: 'http://localhost:5000/sample',
    json: true
};

router.get('/', function(req, res, next) {
    request(options)
        .then(value => {
        console.log("Request successful".green);
        res.send(value);
        })
        .catch(error => {
        console.log(error.red);
        res.send(error);
        });
});



module.exports = router;