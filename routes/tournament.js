//Route that holds all tournament control logic
// --> render tournaments (plan, results, playerlists), start tournaments, access to match resolving

var express = require('express');
var router = express.Router();
var userlogic = require('../service/UserLogic');
var tournamentlogic = require('../service/TournLogic');
//var config = require('config');
var config = require ('../config/default');


/* GET tournament page. */
router.get('/:tournName', function(req, res, next) {
    var tournNameT = req.params.tournName;
    console.log("checking for existence:" + tournNameT);
    //checking for existense to throw usable errors. Might be unneccessary 
    tournamentlogic.exists(tournNameT)
    .then(Texists => {
        if (Texists) {
            console.log("Tournament found");
            //checking whether the tournament is ative to decide which view to render
            tournamentlogic.isActive(tournNameT)
            .then(Tactive => {
                if (Tactive) {
                    //if the tournament is already active, the tournament plan gets rendered including results entered so far
                    console.log("Tournament active");
                    
                    //matchesOut is deprecated
                    var matches = tournamentlogic.getMatches(tournNameT).then(result => {
                        //console.log("Matches found: " + result);
                        //the HTML for the tournament plan and results needs to be created, this is doe here:
                        tournamentlogic.generateTable(tournNameT, result).then(result2 => {
                            res.render('tournamentPlan', {layout: 'main',tournName:tournNameT, /*matchesOut : result,*/ tableData : result2.tableData, resultData : result2.resultData });
                        }).catch (error => {
                            var alertOut = "Oh joy, you broke it! Please report this. Sorry for the inconvenience. " + error;       res.render('alert', {returnURL : "/", alertText : alertOut});
                        });
                    }).catch(error => {var alertOut = "Oh joy, you broke it! Please report this. Sorry for the inconvenience. " + error;       res.render('alert', {returnURL : "/", alertText : alertOut});});
                } else {
                    //if the tournament is still inactive, the playerlist gets rendered where new players can sign up for the tournament
                    tournamentlogic.getPlayer(tournNameT).then(players => {
                        //console.log("rendering tournament: "+req.params.tournName + "spieler: " + players);
                        res.render('playerList', {tournName:tournNameT, playerList: players});
                    }).catch(error => {
                        var alertOut = "Oh joy, you broke it! Please report this. Sorry for the inconvenience. " + error;       res.render('alert', {returnURL : "/", alertText : alertOut});
                    });
                }
            }).catch(error => {
                var alertOut = "Oh joy, you broke it! Please report this. Sorry for the inconvenience. " + error;       res.render('alert', {returnURL : "/", alertText : alertOut});
            });
        }
    }).catch(error => {
        var alertOut = "Oh joy, you broke it! Please report this. Sorry for the inconvenience. " + error;       res.render('alert', {returnURL : "/", alertText : alertOut});
    })
    
    
});

//subroute to render the match result/confirmation page for a certain match._id
router.get('/:tournName/confirm/:matchID', function(req, res, next) {
    console.log("checking for existence:" + req.params.tournName);
    tournamentlogic.exists(req.params.tournName)
    .then(Texists => {
        if (Texists) {
            console.log("Tournament found: " + req.params.tournName);
            //the 'active' validation should be unnecessary since you can only get here for active tournaments
            //get the full match object for further logic
            tournamentlogic.getMatchObject(req.params.tournName, req.params.matchID).then(result => {
                //console.log(result);
                //this logic is necesarry because handelbars had issues accepting Strings within <script>s, where it would think the String was a variable's name
                if (result.confirmed) {
                    //if match is already confirmed, the third confirm view is shown
                    res.render('confirmResult2', {tournName : req.params.tournName, matchID : req.params.matchID, match : result, state : "2", winner : result.winner});
                    //res.render('alert', {'alert'Text : "Match already confirmed! The winner is " + result.winner + "!", returnURL : "/tournament/"+req.params.tournName});
                } else if (result.winner === "" || result.winner === undefined ) {
                    //if no data is entered the first one is shown
                    res.render('confirmResult0', {tournName : req.params.tournName, matchID : req.params.matchID, match : result, state : "0"});
                } else {
                    //otehrwise the second is shown
                    res.render('confirmResult1', {tournName : req.params.tournName, matchID : req.params.matchID, match : result, state: "1", winner : result.winner, confirmedBy : result.confirmedBy});
                }
            }).catch(error => {
                console.log(error);
                var alertOut = "Oh joy, you broke it! Please report this. Sorry for the inconvenience. " + error;       res.render('alert', {returnURL : "/", alertText : alertOut});
            });
        } else {
            var alertOut = "Oh joy, you broke it! Please report this. Sorry for the inconvenience. 'Tournament does not exist.'";
            res.render('alert', {returnURL : "/", alertText : alertOut});
        }
    }).catch(error => {
        var alertOut = "Oh joy, you broke it! Please report this. Sorry for the inconvenience. " + error;       res.render('alert', {returnURL : "/", alertText : alertOut});
    }) 
});

//POST for all control functionality
router.post('/', function(req, res, next) {
    console.log("tournament post aufgerufen mit aktion: " + req.body.action);
    //different action for different POST types --> action as parameter to keep the URL simple and short
    if (req.body.action === "start") {
        //action start generates the tournament plan
        //for this the admin passwort is needed, as there is currently no way of adding players later (planned feature)
        //console.log("Aktion: Plan generieren");
        if (req.body.code === config.adminPW) {
            //calling the actua logic to generate the match
            tournamentlogic.generateMatches(req.body.tournName).then(result => {
                //setting match to active for the GET above
                tournamentlogic.activate(req.body.tournName).then(result => {
                    //going back to the view
                    res.redirect('back'); //reload tournament route
                }).catch(error => {
                    var alertOut = "Oh joy, you broke it! Please report this. Sorry for the inconvenience. " + error;       res.render('alert', {returnURL : "/", alertText : alertOut});
                })
                
            }).catch(error => {
                var alertOut = "Oh joy, you broke it! Please report this. Sorry for the inconvenience. " + error;       res.render('alert', {returnURL : "/", alertText : alertOut});
            });
        } else {
            console.log("wrong code");
            var error = "Wrong password entered!"
            var alertOut = "Oh joy, you broke it! Please report this. Sorry for the inconvenience. " + error;       res.render('alert', {returnURL : "/", alertText : alertOut});
        }
    } else if (req.body.action === "addPlayer") {
        //addPlayer adds new player to playerlist of current tournament
        //new users get registered as needed
        //console.log("Aktion: Spieler hinzufügen");

        //register/look up user without a password
        userlogic.addUser(req.body.playerName, "")
        .then (checkUser => {
            //console.log("aus User promise raus: " + checkUser.exists + " " + checkUser.code);
            //adding the user to the tournament as a player
            tournamentlogic.addPlayer(req.body.tournName, req.body.playerName)
            .then(result => {
                //console.log("aus tournament promise raus: " + result);
                if (checkUser.exists) {
                    //if it's an existing user, you immediately get redirected back
                    console.log("USer existiert, reload");
                    res.redirect('back'); //reload tournament route
                } else {
                    //otherwise you'll get an alert with the confirmation key
                    console.log("Neuer Nutzer!");
                    var out = "Welcome, new player! Please remember the following key, you WILL need it to participate! Your key is: " + checkUser.code;
                    returnString = "/tournament/" + req.body.tournName;
                    res.render('alert', {returnURL : returnString, alertText: out});
                }
            }).catch(error => {
                var alertOut = "Oh joy, you broke it! Please report this. Sorry for the inconvenience. " + error;       res.render('alert', {returnURL : "/", alertText : alertOut});
            });
            
        }).catch (error => {
            var alertOut = "Oh joy, you broke it! Please report this. Sorry for the inconvenience. " + error;       res.render('alert', {returnURL : "/", alertText : alertOut});
        });
    } else if (req.body.action === "confirm") {
        //console.log(req.body);
        //logic to decide what to do with the match
        //states: 0=no result entered, 1=winner entered but nt yet confirmed, 2=confirmed and done
        if (req.body.state == "0") {
            //logic to enter a winner
            tournamentlogic.resolveMatch(req.body.tournName, req.body.matchID, req.body.key, req.body.winner).then(result => {
                res.redirect("/tournament/" + req.body.tournName);
            }).catch(error =>{
                if (error ==="wrong key") {
                    returnString = "/tournament/" + req.body.tournName + "/confirm/" + req.body.matchID;
                    res.render('alert', {returnURL : returnString, alertText : "Key is not authorized to enter/confirm this result."});
                } else {
                    var alertOut = "Oh joy, you broke it! Please report this. Sorry for the inconvenience. " + error;       res.render('alert', {returnURL : "/", alertText : alertOut});
                }
            });
        } else if (req.body.state == "1") {
            //logic to confirm result with second key
            tournamentlogic.resolveMatch(req.body.tournName, req.body.matchID, req.body.key, "").then(result => {
                res.redirect("/tournament/" + req.body.tournName);
            }).catch(error =>{
                if (error ==="wrong key") {
                    returnString = "/tournament/" + req.body.tournName + "/confirm/" + req.body.matchID;
                    res.render('alert', {returnURL : returnString, alertText : "Key is not authorized to enter/confirm this result."});
                } else {
                    var alertOut = "Oh joy, you broke it! Please report this. Sorry for the inconvenience. " + error;       res.render('alert', {returnURL : "/", alertText : alertOut});
                }
            });
        } else {
            //literally nothing.
            //planned feature: admin access to reset result
            res.redirect("/tournament/" + req.body.tournName);
        }
    }
});

module.exports = router;