//deprecated
//current status: all admin functionality is done within the actual routes, e.g. generation of tournament via the tournamentplan view
//current plan: continue doing admin functionality within main form, e.g. match reset within match form

var express = require('express');
var router = express.Router();
/* var config = require('config'); config.get('adminPW')) */
var config = require ('../config/default');

/* GET home page. */
router.get('/userkey', function(req, res, next) {
    if (req.body.username && req.body.password === config.adminPW ){
        User.findOne({loginname: req.body.username})
        .then(function(result){
            if(result === null){
                res.send("MISSING");
                
            } else {
                res.send(result.key);
            }
        }).catch(error => {
            console.log("An error occured while checking logindata");
            res.send("ERROR");
        });
    }
    
    else {
        res.send('FAILED');
    }
});

module.exports = router;
