//Home page/Index
//includes functionality to create new tournaments

var request = require('request-promise');
var express = require('express');
var layout = require('express-layout');
var bodyParser = require('body-parser');
var router = express.Router();
var User = require('../models/user');
var tournamentlogic = require('../service/TournLogic');
var urlencodedParser = bodyParser.urlencoded({ extended: false })

// render the home page
router.get('/', function (req, res, next) {
   res.render('index', {layout:'main', title:'home'});
});

//POST to create new tournaments from the Index page'S form
router.post('/', urlencodedParser, function (req, res, next){
  //console.log(req.body);
  var Iname = req.body.Iname
  //console.log(Iname);
  if (Iname == undefined){
    //catch an emtpy input
    console.log("Kein Name");
    var alertOut = "Oh joy, you broke it! Please report this. Sorry for the inconvenience. 'Tournament name not set.' Second thought: Don't report this. This is just you being too dumb.";
    res.render('alert', {returnURL : "/", alertText : alertOut});
  }
  else {
    //logic to create tourny
    console.log("Tournamentname: " + Iname);
    tournamentlogic.generate(Iname).then(result => {
      //on success call page fo new Tourny
      //this makes this input double as a 'open tournamnet'
      res.redirect('/tournament/'+Iname);
    }).catch(error => {
      var alertOut = "Oh joy, you broke it! Please report this. Sorry for the inconvenience. " + error;
      res.render('alert', {returnURL : "/", alertText : alertOut});
    });
  }
});


module.exports = router;