//this service contains all logic needed for user management
var User = require('../models/user');

class UserLogic { 

    /*
    #############
    deprecated, the 'no password' function is no longer used
    #############
    
    addUser(Iname) {
        User.findOne({name:Iname})
                .then(function(result){
                    console.log("promise");
                    if(result === null){
                        console.log("neuen Nutzer anlegen");
                        var newCode = Math.random().toString().substring(2,4);
                            User.create({name:Iname, code:newCode, password:"", wins:0, losses:0})
                            .then(function(create){
                            })
                            .catch(error => {
                                console.log("An error occured while creating the user");
                            });
                            console.log("create");
                            return {exists:false, code:newCode};
                    }else{
                        console.log("User already exists");
                        return {exists:true, code: ""};  
                    }
                })
                .catch(error => {
                    console.log("An error occured while checking logindata");
                })
    };*/

    //checking if a User exists, if not, we'll create one
    addUser(Iname, Ipassword) {
        //console.log("in USerLogic");
        return new Promise ((resolve, reject) => {
            //console.log("im Promise, suchen nach: '" + Iname + "'");
            //checking for existence
            User.findOne({name:Iname})
            .then(function(result){
                //console.log("DB promise");
                if(result === null){
                    //new user, creating one
                    console.log("neuen Nutzer anlegen");
                    //generating new key from a random number via substring
                    var newCode = Math.random().toString().substring(2,5);
                        //setting default data
                        User.create({name:Iname, code:newCode, password:Ipassword, wins:0, losses:0})
                        .then(function(create){
                            console.log("create");
                            //returning the code with existence as false
                            resolve({exists:false, code:newCode});
                        })
                        .catch(error => {
                            console.log("An error occured while creating the user: " + error);
                            reject("error creating user");
                        });
                } else{
                    //user exists, returning existence as true
                    console.log("User already exists");
                    resolve ({exists:true, code: ""});  
                }
            })
            .catch(error => {
                console.log("An error occured while checking logindata");
                reject("error checking username");
            })
        });
        
    };

    //adding a win or loss to a user
    addResult(Iname, type) {
        return new Promise ((resolve, reject) => {
            User.findOne({name:Iname})
            .then(function(result){
                console.log("promise");
                if(result === null){
                    reject ("404");
                }else{
                    if(type === "win") {
                        var winT = result.wins + 1;
                        User.update({name:Iname},{wins:winT});
                        //debug code:
                        user.findOne({name:Iname}).then(newUser => {
                            console.log(newUser);
                        }).catch(error);
                        //end debug code
                        resolve( "win added"); 
                    } else {
                        var lossT = result.losses + 1;
                        User.update({name:Iname},{losses:lossT});
                        resolve ("loss added"); 
                    }
                }
            })
            .catch(error => {
                console.log("An error occured while checking logindata");
                reject(error);
            });
        });
    }

    //utility to generate a new userkey
    //currently not used
    newKey(Iname) {
        User.findOne({name:Iname})
                .then(function(result){
                    console.log("promise");
                    if(result === null){
                        return "User does not exist";
                    }else{ 
                        var newCode = Math.random().toString().substring(2,4);
                        User.update ({name:Iname},{code:newCode});
                        return newCode;
                    }
                })
                .catch(error => {
                    console.log("An error occured while checking logindata");
                })
    }
    
    //login, currently unused
    login(Iname, Ipassword) {    
                User.findOne({name:Iname})
                    .then(function(result) {
                        if(result === null) {
                            return "User does not exist";
                        }
                        else {
                            if (Ipassword === result.password) {
                                return result.code;
                            } else {
                                return "wrong password"
                            }
                        }   
                })
                .catch(function(err) { 
                    res.send("error looking for user with loginname");
                });
    };

    //simple function to return suer object from it's name
    getUserFromName(Iname) {
        return new Promise((resolve, reject) => {
            console.log("lokkong for user with the name: " + Iname);
            User.findOne({name:Iname})
            .then(function(result) {
                if(result === null) {
                    console.log("User not found.");
                    reject( "User does not exist");
                }
                else {
                    //console log to find key for debug purposes
                    console.log(result);
                    resolve (result);
                }   
            })
            .catch(function(err) { 
                console.log(err);
                reject("error: " + err);
            });
        })
        
    }
    
};

module.exports=new UserLogic();