//main service for all tournament related logic

var tournament = require('../models/tournament');
var match = require('../models/match');
var user = require('../models/user');
var robin = require('roundrobin');
var userlogic = require('../service/UserLogic');
var ObjectId = require('mongoose').Types.ObjectId;


class TournLogic { 

    //simple function to check for exitence
    //returns true or false
    exists(tournName) {
        return new Promise((resolve, reject)=> {
            tournament.findOne({name:tournName})
            .then(result => {
                if (result === null) {
                    console.log("not found");
                    resolve(false);
                } else {
                    console.log("tournament found.")
                    resolve(true);
                }
            }).catch(error => {
                console.log("An error occured while addig player to Tournament: " + error);
                reject("An error occured while addig player to Tournament: " + error);
            });
        });
    };

    //simple function to check for it's activation
    //returns true or false
    isActive(tournName) {
        return new Promise ((resolve, reject) => {
            tournament.findOne({name:tournName})
            .then(function(result) {
                if (result === null) {
                    console.log("not found");
                } else {
                    if(result.active === true) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                }
            }).catch(error => {
                console.log("An error occured while addig player to Tournament: " + error);
                reject("An error occured while addig player to Tournament: " + error);
            });
        });
    };

    //simply sets the touraments 'active' boolean to true
    activate(tournName) {
        return new Promise((resolve, reject) => {
            tournament.update({name:tournName},{active:true})
            .then(result => {
                resolve ("sucess");
            }).catch(error => {
                console.log(error);
                reject(error);
            });
        });
    }

    //function to create a new tournament
    generate(Iname) {
        return new Promise((resolve, reject) => {
            var Tname = Iname;
            console.log("temp name: " + Tname);
            //filling it with start values
            var tournT = { name: Tname , players: [], matches: [], active:false};
            tournament.create(tournT)
            .then(function(create){
                console.log("Successfully generated new Tournament: " + tournT.name);
                resolve();
            })
            .catch(error => {
                console.log("An error occured while creating the Tournament " + error);
                reject(error);
            });
        });
    };

    //function to add existing user to Tournament as a player
    addPlayer(tournName, player) {
        //console.log("in Tounament Logik");
        return new Promise ((resolve, reject) => {
            //console.log("im tournament promise für " + tournName +" und " + player);
            tournament.findOne({name:tournName})
            .then(function(result) {
                //console.log("abfrage durchgeführt");
                if (result === null) {
                    console.log("not found");
                    reject("tournament not found");
                } else {
                    //getting and updating the players array
                    var playersT = result.players;
                    //check for duplicates
                    var dupe = false;
                    for (usr of playersT) {
                        if (usr === player) {
                            console.log("duplicate");
                            dupe = true;
                        }
                    }
                    if (dupe) {
                        reject("duplicate");
                    } else {
                        //update of list
                        console.log("verscuhe auf liste zu pushen: " + playersT);
                        playersT.push(player);
                        //update of tourny with new list
                        //lässt sich vielleicht auch über subdocs regeln?
                        tournament.update({name:tournName},{players:playersT})
                        .then(result => {
                            console.log("player " + player + "added to Tournament " + tournName);
                            resolve ("success");
                        }).catch(error => {
                            reject(error);
                        });
                    }                    
                }
            }).catch(error => {
                console.log("An error occured while addig player to Tournament: " + error);
                reject("An error occured while addig player to Tournament: " + error);
            });
        });        
    };

    //generates match objects and tourny plan
    generateMatches(tournName) {
        //console.log("in tounLogic: " + tournName);
        return new Promise ((resolve, reject) => {
            //checking for tourn existence
            tournament.findOne({name:tournName})
            .then(function(result) {
                //console.log("request durch " + result);
                if (result === null) {
                    console.log("no found");
                    reject("404");
                } else {
                    console.log("tournament found");
                    //extracting playerlist
                    var playersT = result.players;
                    var planT = [];
                    var matchesT = []
                    //generating tournament plan as {round 1, round 2} where each round {match 1, match 2} and each match [player1, player2]
                    var robined = robin(playersT.length, playersT);
                        //logic to generate tournament table
                        //generate the match objects
                        for (var round of robined) {
                            console.log("Round has this content: " + round);
                            var roundT = [];
                            for (var matchString of round) {
                                var nameT = matchString[0] + " v " + matchString[1];
                                var matchT = new match ({
                                    name : nameT,
                                    player1 : matchString[0],
                                    player2 : matchString[1],
                                    winner : "",
                                    confirmed : false,
                                    confirmedBy : ""});
                                    //full object gets pushed on match lsit
                                matchesT.push(matchT);
                                //match._id gets pushed on tourn plan
                                roundT.push(matchT.id);
                            }
                            planT.push(roundT);
                        }
                        //writing data back
                    tournament.update({name:tournName},{matches:matchesT, plan:planT}).then(result => {
                        console.log("Tournament generated: "+ matchesT);
                            resolve (robined);
                    }).catch (error =>{
                        console.log(error);
                        reject(error);
                    })  
                }
            }).catch(error => {
                console.log("An error occured while generating Tournament: " + error);
                reject (error);
            });
        })
        
    };

    //simple function to return the tourn plan
    getMatches(tournName) {
        return new Promise ((resolve, reject) => {
            tournament.findOne({name:tournName})
            .then(function(result) {
                if (result === null) {
                    console.log("no found");
                    reject("404");
                } else {
                    //console.log("Matches found for Tournament: " + tournName + "; " + result);
                    resolve (result.plan);
                }
            }).catch(error => {
                console.log("An error occured while getting Tournament: " + error);
                reject(error);
            });
        });
        
    };

    //simple function to convert a match._id in the full object
    getMatchObject(tournName, matchID) {
        return new Promise ((resolve, reject) => {
            console.log("in promise für " + tournName + ", suche nach match mit ID " + matchID);
            tournament.findOne({name:tournName})
            .then(function(result) {
                console.log("found: " + result.matches);
                //mongoose Mmethod document in docs not working -.-
                //var doc = result.matches.id(matchID);
                //workaround:
                for (var matchT of result.matches) {
                    if (matchT._id == matchID) {
                        resolve(matchT);
                    }
                }
                reject("Match not found?");
            }).catch(error => {
                console.log(error);
                reject(error);
            });
        });
    }

    //simple function to return the playerlist
    getPlayer(tournName) {
        return new Promise ((resolve, reject) => {
            tournament.findOne({name:tournName})
            .then(function(result) {
                if (result === null) {
                    console.log("no found");
                } else {
                    console.log("rückgabe: " + result.players);
                    resolve(result.players);
                }
            }).catch(error => {
                console.log("An error occured while getting Tournament: " + error);
                reject("An error occured while getting Tournament: " + error);
            });
        });
        
    };

    //resolving a match: entering a winner/confirming result
    resolveMatch(tournName, matchID, key, winner) {
        return new Promise ((resolve, reject) => {
            //console.log("in T Logic");
            tournament.findOne({name:tournName})
            .then(function(result) {
                if (result === null) {
                    console.log("not found");
                    reject("404");
                } else {
                    console.log("Tournament found");
                    //getting to correct match out of the list
                    //coudl prob be done with subdocs, TODO
                    var result2;
                    for (var matchT of result.matches) {
                        if(matchT._id == matchID) {
                         result2 = matchT;
                        break;
                        }
                    }
                    //console.log(result2);
                    //console.log("winner is '" + result2.winner + "'");

                    //differenciating between actions:
                    if (result2.winner === "" || result2.winner === undefined) {
                        //if the match has no result, we'll assume it's the initial entering of a winner
                        //we need both players as objects then
                        userlogic.getUserFromName(result2.player1).then (player1Object => {
                            userlogic.getUserFromName(result2.player2).then (player2Object => {
                                console.log("checking if key is authorized " + key);
                                //figure out of one of their keys is correct
                                if(player1Object.code === key || player2Object.code === key) {
                                    //figure out which player the key is actually from and save the OTHER player for later use
                                    var userT;
                                    if(player1Object.code === key) {
                                        userT = player2Object.name;
                                    } else {
                                        userT = player1Object.name;
                                    }
                                    console.log("authorized, " + userT + " needs to confirm result.");
                                    //updating the match object
                                    tournament.findOneAndUpdate(
                                    { "name": tournName, "matches._id": new ObjectId(matchID) },
                                    { 
                                        "$set": {
                                            "matches.$.winner": winner,
                                            "matches.$.confirmed": false,
                                            //since we know which player the key was from, we can assume the other one has to confirm the result
                                            "matches.$.confirmedBy": userT
                                        }
                                    }).then(result => {
                                        console.log(result);
                                        resolve(result);
                                    }).catch(error => {
                                        console.log(error);
                                        reject(error);
                                    });
                                } else {
                                    console.log("wrong key");
                                    reject ("wrong key");
                                }
                            }).catch(error => {
                                console.log("error: " + error);
                                reject(error);
                            });
                        }).catch(error => {
                            console.log("error: " + error);
                            reject(error);
                        });
                    } else {
                        //if the match has a winner entered, we'll assume the action is to confirm it
                        //we only need the player that has to confirm teh result then:
                        userlogic.getUserFromName(result2.confirmedBy).then (player1Object => {
                                //console.log("checking if key is authorized " + key);
                                //checking his key
                                if(player1Object.code === key) {
                                    console.log("authorized, " + player1Object.name + " IS confirming result.");
                                    //confirming th match
                                    tournament.findOneAndUpdate(
                                    { "name": tournName, "matches._id": new ObjectId(matchID) },
                                    { 
                                        "$set": {
                                            "matches.$.confirmed": true
                                        }
                                    }).then(result => {
                                        console.log(result);
                                        //TODO: adding losses to user stats
                                        //currently adding wins to user stats:
                                        //userlogic.addResult(result.winner, "win").then(someValue => {
                                        //    console.log(someValue);
                                            resolve(result);
                                        //}).catch (error => {
                                        //    console.log(error);
                                        //    reject(error);
                                        //});
                                    }).catch(error => {
                                        console.log(error);
                                        reject(error);
                                    });
                                } else {
                                    console.log("wrong key");
                                    reject ("wrong key");
                                }
                        }).catch(error => {
                            console.log("error: " + error);
                            reject(error);
                        });
                    }
                }
            }).catch(error => {
                console.log("An error occured while resolving match: " + error);
                reject (error);
            });
        });
    };


    /*
    ############
    deprecated, test fuction
    ############

        confirmResult(tournName, matchID, key) {
            return new Promise ((resolve, reject) => {
                console.log("in T Logic");
                tournament.findOne({name:tournName})
                .then(function(result) {
                    if (result === null) {
                        console.log("not found");
                    } else {
                        console.log("Tournament found");
                        var result2;
                        for (var matchT of result.matches) {
                            if(matchT._id == matchID) {
                             result2 = matchT;
                            break;
                            }
                        }
                        console.log(result2);
                        console.log("winner is '" + result2.winner + "'");
                        if (result2.winner === "" || result2.winner === undefined) {
                            userlogic.getUserFromName(result2.player1).then (player1Object => {
                                userlogic.getUserFromName(result2.player2).then (player2Object => {
                                    console.log("checking if key is authorized " + key);
                                    if(player1Object.code === key || player2Object.code === key) {
                                        var userT;
                                        if(player1Object.code === key) {
                                            userT = player2Object.name;
                                        } else {
                                            userT = player1Object.name;
                                        }
                                        console.log("authorized, " + userT + " needs to confirm result.");
                                        /*var idT = new ObjectId(matchID);
                                        console.log(idT);
                                        tournament.findOne({"name":tournName, "matches._id": idT}).then(result => {
                                            console.log(tournName + ", " + matchID + ", als matchID: '" + idT + "', " + result);
                                        }).catch(error => { console.log("found error: " + error);});
                                        tournament.findOneAndUpdate(
                                        { "name": tournName, "matches._id": new ObjectId(matchID) },
                                        { 
                                            "$set": {
                                                "matches.$.winner": winner,
                                                "matches.$.confirmed": false,
                                                "matches.$.confirmedBy": userT
                                            }
                                        }/*,
                                        function(err,doc) {
                                            //console.log("doc: " + doc + ", err: " + err);
                                            //resolve(doc);
                                        }).then(result => {
                                            console.log(result);
                                            resolve(result);
                                        }).catch(error => {
                                            console.log(error);
                                            reject(error);
                                        });
                                    } else {
                                        console.log("wrong key");
                                        reject ("wrong key");
                                    }
                                }).catch(error => {
                                    console.log("error: " + error);
                                    //error
                                });
                            }).catch(error => {
                                console.log("error: " + error);
                                //error
                            });
                        }
                        
                        // Irgendwie Mtach identifizieren
                        //tournament.update({name:tournName},{});
                        //console.log("player " + player + "added to Tournament " + tournName);
                    }
                }).catch(error => {
                    console.log("An error occured while resolving match: " + error);
                    reject (error);
                });
            });
        
        
    };*/

    //function to generate the HTML for the tournamentplan view
    generateTable(tournName, matches) {
        return new Promise ((resolve, reject) => {
            //getting the tourny data
        tournament.findOne({name:tournName})
        .then(function(result) {
            //starting up two HTML tables
            let tableO = ["'<table border=1>"];
            let table1 = ["'<table border=1>"];
            var winnerList = [];
            //iterate over matches and generate tables
            for(let row of matches) {
                //adding row tags
                    tableO.push("<tr>");
                    table1.push("<tr>");
                    for(let cell of row){
                            //getting the match object
                            //not working as defined in doc:
                            //var result2 = result.matches.id(cell);
                            //hence this stupid loop for each run:
                            var result2;
                            for (var matchT of result.matches) {
                                if(matchT._id == cell) {
                                    result2 = matchT;
                                    break;
                                }
                            }
                            if (result2.confirmed) {
                                //if a mtach is confirmed we're pushing
                                //the winner to the winnerlist
                                winnerList.push(result2.winner);
                                //a label in the tourn plan
                                tableO.push(`<td>${result2.name}</td>`);
                                //the winner in the result table
                                table1.push(`<td>${result2.winner}</td>`);
                            } else {
                                //otherwise, we push
                                //the confirmation html form in the plan
                                tableO.push(`<td><form method="GET" action="/tournament/${tournName}/confirm/${result2._id}" enctype="application/x-www-form-urlencoded"><input type="submit" value="${result2.name}"></form></td>`);
                                //and a blank in the result table
                                table1.push(`<td>Result not entered.</td>`);
                            }
                    }
                    //ending row tag
                    tableO.push("</tr>");
                    table1.push("</tr>");
            }
            //ending tables and joining the array to generate working HTML code
            tableO.push("</table>'");
            tableO.join('\n');
            table1.push("</table>");
            table1.join('\n');

            //convert list of winners within the matches into usable stats
            //for now: number of wins
            //code based on Rex's answer: https://stackoverflow.com/questions/19395257/how-to-count-duplicate-value-in-an-array-in-javascript
            //accumulating the number of times a person is in this list
            winnerList.sort();
            var current = null;
            var cnt = 0;
            var wins = {};
            for (var i = 0; i < winnerList.length; i++) {
                if (winnerList[i] != current) {
                    if (cnt > 0) {
                        if (current != '') wins[current] = {name: current, wins: cnt};
                    }
                    current = winnerList[i];
                    cnt = 1;
                } else {
                    cnt++;
                }
            }
            if (cnt > 0) {
                if (current != '') wins[current] = {name: current, wins: cnt};
            }
            //console.log(wins);
            //von Nenad auf https://stackoverflow.com/questions/38824349/convert-object-to-array-in-javascript/44790922
            //sorting by number of wins
            var sorted = Object.keys(wins).map(function(key) {
                return [Number(key), wins[key]];
            });
            //console.log(sorted);
            sorted.sort(function(a,b) {
                return a.wins - b.wins;
            });
            var list = [];
            //console.log(sorted);
            //pushing it as HMTL to an out array
            for (user of sorted) {
                console.log(user);
                list.push(`<li>${user[1].name} has ${user[1].wins} wins.</li>`);
            }
            //ending the html and generating the final output string
            list.push("'");
            list.join('\n');
            var resultOut = table1 + list;
            //begone, Thot!
            resolve ({tableData : tableO, resultData : resultOut});
        }).catch(error => {
            console.log(error);
            reject (error);
        });

        });
    }

};

module.exports=new TournLogic();