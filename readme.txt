CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * FAQ

 INTRODUCTION
------------
Der Tournament Generator ist eine Anwendung, welche die Erstellung, Verwaltung und Auswertung von Tournaments erleichtert. Vorrangig sollen damit 1v1-Tournaments im Spiel Mech Warrior Online umgesetzt werden.
Bei dieser Anwendung handelt es sich um einen Container, welcher das Web-Tool erzeugt, und um einen Container mit einer MongoDB, in der die Tunierinformationen gespeichert werden.

 REQUIREMENTS
------------
Damit der Tournament Generator betrieben werden kann, sind mehrere Komponenten notwendig: 
    - ARM-Prozessor 
    - Node.js
    - Docker
    - Git

INSTALLATION
------------
Um die Anwendung verwenden zu können, muss zuerst Node.js und Docker installiert werden.
Zuerst muss ein Container mit einer MongoDB gestartet werden. Dazu muss in der Konsole der Befehl: docker pull jixer/rpi-mongo verwendet werden. Nachdem der Container heruntergeladen wurde, kann per Docker run -d jixer/pri-mongo die MongoDB gestartet werden.
Sobald die MongoDB gestartet ist, kann der Container des Web-Tools gestartet werden.
Anschließend muss das Dockerfile aus dem Projektordner heruntergeladen werden und gespeichert werden. Dort, wo das Dockerfile gespeichert wurde, muss dann eine Konsole geöffnet werden, um mit Befehl docker image build tourn . ein Image für den Container des Web-Tools zu erstellen.
Mit dem Befehl "docker run -d tourn" kann nun der Container gestartet werden.

CONFIGURATION
------------
Es können zwei Teile konfiguriert werden: die Ports und das Adminpasswort. 
Um die Ports der Anwendung und der Datenbank ändern zu können, ist es notwendig, denn Projektordner herunterzuladen. In diesem muss dann die Datei "/bi/www" angepasst werden. 
Das Adminpasswort ist in der Datei "/config/default.js" eingefügt und kann dort geändert werden.

FAQ
------------
soon (tm)