//Interface to load the match Schema and turn it into a usable model
//Interface to load the tournament Schema and turn it into a usable model

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TournamentSchema = require('./schema/tournament.js');

var Tournament = mongoose.model('tournament', TournamentSchema);

module.exports = Tournament;