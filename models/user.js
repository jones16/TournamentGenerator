//Interface to load the user/player Schema and turn it into a usable model
//
//the user schema is saved as json vs the js files for other schema, hence the extra function here.
//it mostly serves as a demo, therefore we're keeping the two different methods even though it's inconsitent.

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var iuser = require('./schema/user.json');

var UserSchema = new Schema(
    iuser
);

var User = mongoose.model('user', UserSchema);

module.exports = User;