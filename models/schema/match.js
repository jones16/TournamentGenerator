var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var user = require('./user');

var MatchSchema = new Schema(

    
{
    //name of match, e.g. A v. B
    "name": {
        "required": true,
        "type": "String",
        "unique": false
    },
    //players being part of the match
    "player1": {
        "required": true,
        "type": "String",
        "unique": false
    },
    "player2": {
        "required": true,
        "type": "String",
        "unique": false
    },
    //winner of the match
    "winner": {
        "type": "String",
        "required": false,
        "unique": false
    },
    //whether the match is confirmed by both parties
    "confirmed": {
        "type" : "Boolean",
        "required": false,
        "unique": false
    },
    //the person to confirm the result the other person has entered
    "confirmedBy" : {
        "type" : "String",
        "required": false,
        "unique": false
    }
});

module.exports = MatchSchema;