var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TournamentSchema = new Schema({
    name: {
        required: true,
        type: "Object",
        unique: true
    },
    active: {
        required: true,
        type: "Boolean",
        unique: false
    },
    players: {
        required: true,
        type: "Object",
        unique: false
    },
    //list of actual match objects --> mongoose subdoc
    matches: {
        required: false,
        type: "Object",
        unique: false
    },
    //tournament plan, saved as {round1, round2}, where each round is {match1, match2} etc. matches are declared using their mongoose ._id
    plan: {
        required: false,
        type: "Object",
        unique: false
    }

});

module.exports = TournamentSchema;